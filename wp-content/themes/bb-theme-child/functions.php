<?php

// Defines
define('FL_CHILD_THEME_DIR', get_stylesheet_directory());
define('FL_CHILD_THEME_URL', get_stylesheet_directory_uri());

remove_action('wp_head', 'wp_generator');
if (current_user_can('manage_options')) {
    add_filter('show_admin_bar', '__return_true');
}
// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action('wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000);

add_action('wp_enqueue_scripts', function () {
    wp_enqueue_script("slick", get_stylesheet_directory_uri() . "/resources/slick/slick.min.js", "", "", 1);
    wp_enqueue_script("cookie", get_stylesheet_directory_uri() . "/resources/jquery.cookie.js", "", "", 1);
    //wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);
});


// Register menus
function register_my_menus()
{
    register_nav_menus(
        array(
            'footer-1' => __('Footer Menu 1'),
            'footer-2' => __('Footer Menu 2'),
            'footer-3' => __('Footer Menu 3'),
            'footer-4' => __('Footer Menu 4'),
            'footer-5' => __('Footer Menu 5'),
            'site-map' => __('Site Map'),
        )
    );
}
add_action('init', 'register_my_menus');



// Enable shortcodes in text widgets
add_filter('widget_text', 'do_shortcode');


// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');





function fr_img($id = 0, $size = "", $url = false, $attr = "")
{

    //Show a theme image
    if (!is_numeric($id) && is_string($id)) {
        $img = get_stylesheet_directory_uri() . "/images/" . $id;
        if (file_exists(to_path($img))) {
            if ($url) {
                return $img;
            }
            return '<img src="' . $img . '" ' . ($attr ? build_attr($attr) : "") . '>';
        }
    }

    //If ID is empty get the current post attachment id
    if (!$id) {
        $id = get_post_thumbnail_id();
    }

    //If Id is object it means that is a post object, thus retrive the post ID
    if (is_object($id)) {
        if (!empty($id->ID)) {
            $id = $id->ID;
        }
    }

    //If ID is not an attachment than get the attachment from that post
    if (get_post_type($id) != "attachment") {
        $id = get_post_thumbnail_id($id);
    }

    if ($id) {
        $image_url = wp_get_attachment_image_url($id, $size);
        if (!$url) {
            //If image is a SVG embed the contents so we can change the color dinamically
            if (substr($image_url, -4, 4) == ".svg") {
                $image_url = str_replace(get_bloginfo("url"), ABSPATH . "/", $image_url);
                $data = file_get_contents($image_url);
                echo strstr($data, "<svg ");
            } else {
                return wp_get_attachment_image($id, $size, 0, $attr);
            }
        } else if ($url) {
            return $image_url;
        }
    }
}
//Facet Title Hook
add_filter('facetwp_shortcode_html', function ($output, $atts) {
    if (isset($atts['facet'])) {
        $output = '<div class="facet-wrap"><strong>' . $atts['title'] . '</strong>' . $output . '</div>';
    }
    return $output;
}, 10, 2);

/**
 * Dequeue the jQuery UI styles.
 *
 * Hooked to the wp_print_styles action, with a late priority (100),
 * so that it is after the style was enqueued.
 */
function remove_pagelist_css()
{
    wp_dequeue_style('page-list-style');
}
add_action('wp_print_styles', 'remove_pagelist_css', 100);
//add method to register event to WordPress init

add_action('init', 'register_daily_mysql_bin_log_event');

function register_daily_mysql_bin_log_event()
{
    // make sure this event is not scheduled
    if (!wp_next_scheduled('mysql_bin_log_job')) {
        // schedule an event
        wp_schedule_event(time(), 'daily', 'mysql_bin_log_job');
    }
}

add_action('mysql_bin_log_job', 'mysql_bin_log_job_function');


function mysql_bin_log_job_function()
{

    global $wpdb;
    $yesterday = date('Y-m-d', strtotime("-1 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'";
    $delete_endpoint = $wpdb->get_results($sql_delete);
}
add_filter('auto_update_plugin', '__return_false');


//Yoast SEO Breadcrumb link - Changes for PDP pages
add_filter("wpseo_breadcrumb_links", "instockoverride_yoast_breadcrumb_pdp_page");

function instockoverride_yoast_breadcrumb_pdp_page($links)
{
    $post_type = get_post_type();

    if (is_singular('instock_carpet')) {

        $breadcrumb[] = array(
            'url' => get_site_url() . '/in-stock/',
            'text' => 'In-Stock',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/in-stock/carpet-products/',
            'text' => 'Instock Carpet Products',
        );
        array_splice($links, 1, -1, $breadcrumb);
    }

    if (is_singular('instock_lvt')) {

        $breadcrumb[] = array(
            'url' => get_site_url() . '/in-stock/',
            'text' => 'In-Stock',
        );
        $breadcrumb[] = array(
            'url' => get_site_url() . '/in-stock/lvt-products/',
            'text' => 'Instock Luxury Vinyl Products',
        );
        array_splice($links, 1, -1, $breadcrumb);
    }


    return $links;
}

//theme loop template changes for instock PLP
// add_filter('facetwp_template_html', function ($output, $class) {

//     $prod_list = $class->query;
//     ob_start();

//     if (isset($class->query_args['check_instock']) && $class->query_args['check_instock'] == 1) {

//         $dir = get_stylesheet_directory() . '/product-listing-templates/product-loop-new-theme.php';
//     } else {

//         $dir = get_stylesheet_directory() . '/product-listing-templates/product-loop-new.php';
//     }

//     require_once $dir;
//     return ob_get_clean();
// }, 10, 2);


//sigle instock post type template

// add_filter('single_template', 'childtheme_get_custom_post_type_template');

function childtheme_get_custom_post_type_template($single_template)
{
    global $post;

    if ($post->post_type != 'post') {

        // if($post->post_type === 'instock_lvt' || $post->post_type === 'instock_carpet') {

        //     $single_template = get_stylesheet_directory().'/product-listing-templates/single-'.$post->post_type.'.php';        

        // }else{

        $single_template = WP_PLUGIN_DIR . '/mm-retailer-setting/product-listing-templates/single-' . $post->post_type . '.php';
        // }
    }
    return $single_template;
}

//Cron job for sync catalog for all mohawk categories

if (! wp_next_scheduled('sync_mohawk_catalog_all_categories')) {

    wp_schedule_event(strtotime("last Sunday of " . date('M') . " " . date('Y') . ""), 'monthly', 'sync_mohawk_catalog_all_categories');
}


//add_action( 'sync_mohawk_catalog_all_categories', 'mohawk_product_sync', 10, 2 );

function mohawk_product_sync()
{

    write_log("Only mohawk Catalog sync is running");

    global $wpdb;
    $upload = wp_upload_dir();
    $upload_dir = $upload['basedir'];
    $upload_dir = $upload_dir . '/sfn-data';

    $table_posts = $wpdb->prefix . 'posts';
    $table_meta = $wpdb->prefix . 'postmeta';
    $product_json =  json_decode(get_option('product_json'));

    $brandmapping = array(
        "hardwood" => "hardwood_catalog",
        "laminate" => "laminate_catalog",
        "lvt" => "luxury_vinyl_tile",
        "tile" => "tile_catalog"
    );

    foreach ($brandmapping as $key => $value) {

        $productcat_array = getArrayFiltered('productType', $key, $product_json);


        foreach ($productcat_array as $procat) {

            if ($procat->manufacturer == "Mohawk") {

                $permfile = $upload_dir . '/' . $value . '_' . $procat->manufacturer . '.json';
                $res = SOURCEURL . get_option('SITE_CODE') . '/www/' . $key . '/' . $procat->manufacturer . '.json?' . SFN_STATUS_PARAMETER;
                $tmpfile = download_url($res, $timeout = 900);

                if (is_file($tmpfile)) {
                    copy($tmpfile, $permfile);
                    unlink($tmpfile);
                }

                $sql_delete = "DELETE p, pm FROM $table_posts p INNER JOIN $table_meta pm ON pm.post_id = p.ID  WHERE p.post_type = '$value' AND pm.meta_key = 'manufacturer' AND  pm.meta_value = 'Mohawk'";
                write_log($sql_delete);
                $delete_endpoint = $wpdb->get_results($sql_delete);
                write_log("mohawk product deleted");
                //exit;

                write_log('auto_sync - Shaw catalog - ' . $key . '-' . $procat->manufacturer);
                $obj = new Example_Background_Processing();
                $obj->handle_all($value, $procat->manufacturer);

                write_log('Sync Completed - ' . $procat->manufacturer);
            }
        }

        write_log('Sync Completed for all  ' . $key . ' brand');
    }
}
